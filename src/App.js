import './App.css';
import Ticket from './Ticket';
import Extra from './Extra';
import Booking from "./Booking";
import Offer from "./Offer";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
    const dayOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let offers = [new Offer(3,2,100,3)] // This is the offer described on the test doc
    // let offers = []
    let booking = new Booking();
    let ticket = new Ticket('Standart', 7.90, [
        new Extra('Real3D', 0.90),
        new Extra('IMAX', 1.50)
    ])
    booking.addTicket(ticket)

    // ADDING SOME EXTRA TICKETS FOR OFFER TO APPLY
    booking.addTicket(new Ticket('Standart', 7.90))
    booking.addTicket(new Ticket('Standart', 7.90))
    booking.addTicket(new Ticket('Concesion', 5.40))

    booking.applyOffers(offers)

    return (
        <div className="App container">
            <div className={"row"}>
                <div className={"col-10 offset-1 text-right"}>
                    <h1>Checkout System</h1>
                    <p>No UI was required on the test, so all variables can be defined on <i>'App.js'</i>.
                        All the data is contained in a <i>Booking</i> object, whose calculate totals and applies offers.
                        This checkout system applies offers sorting tickets by price in an ascending order.
                        Offers are defined on the App.js object and passed as a parameter to <i>booking.applyOffers()</i>.

                    </p>
                </div>
                <div className={"col-6 offset-3"}>
                <table className={"table table-sm col-4"}>
                    <tbody style={{textAlign: "left"}}>
                    <tr>
                        <th>Tickets</th>
                        <td>
                             {booking.tickets.map((element, i) => {
                                 return <span>{i} => {element.name} - £{element.price} - Extras: {JSON.stringify(element.extras)}<br/></span>
                             })}
                        </td>
                    </tr>
                    <tr>
                        <th>Offers array</th>
                        <td>
                             {offers.map((element) => {
                                 return <span>Min amount of tickets: {element.minimumTickets}<br/>
                                     Tickets offer is applied to: {element.chargedTickets}<br/>
                                     Discount applied (%): {element.discount}<br/>
                                     Day of the week offer applies: {dayOfWeek[element.weekday]}<br/>
                                 </span>
                             })}
                        </td>
                    </tr>
                    <tr>
                        <th>Booking total</th>
                        <td>£{booking.totalPrice}</td>
                    </tr>
                    <tr>
                        <th>Discount</th>
                        <td>£{booking.getDiscount()}</td>
                    </tr>
                    <tr>
                        <th>Final price</th>
                        <td>£{booking.discountedPrice}</td>
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    );
}

export default App;
