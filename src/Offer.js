class Offer {
    constructor(minimum, charged, discount, weekday) {
        this.minimumTickets = minimum
        this.chargedTickets = charged
        this.discount = discount
        this.weekday = weekday
    }
}

export default Offer;
