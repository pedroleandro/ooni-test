class Booking {
    constructor() {
        this.tickets = []
        this.totalPrice = 0
        this.discountedPrice = 0
    }

    addTicket(ticket) {
        this.tickets.push(ticket)
    }

    calculateTotal() {
        let total = 0
        this.tickets.forEach(element =>
            total += element.getTotal()
        )
        this.totalPrice = total
    }

    getDiscount() {
        if (this.totalPrice < 0.01) this.calculateTotal();
        let total = this.totalPrice - this.discountedPrice
        return Math.round(total * 100) / 100
    }

    applyOffers(offers) {
        if (this.totalPrice === 0) {
            this.calculateTotal()
        }

        this.tickets.sort(function (a, b) {
            if (a.getTotal() < b.getTotal()) {
                return 1;
            }
            if (a.getTotal() > b.getTotal) {
                return -1;
            }
            return 0;
        });

        let total = this.totalPrice
        let tickets = JSON.parse(JSON.stringify(this.tickets))
        const dotw = (new Date()).getDay()

        offers.forEach(offer => {
            if (tickets.length >= offer.minimumTickets && dotw === offer.weekday) {
                for (let i = 0; i < offer.chargedTickets; i++) {
                    let tempticket = tickets.pop()
                    total -= (tempticket.price * offer.discount / 100) // Offer discount goes from 100% to 0%
                }
            }
        })
        this.discountedPrice = Math.round(total * 100) / 100
    }
}

export default Booking;
