class Ticket {
    constructor(name, price, extras) {
        this.name = name
        this.price = price
        this.extras = extras ? extras : []
    }

    getTotal() {
        let total = this.price
        this.extras.forEach(element =>
            total += element.price
        )
        return total
    }

    addExtra(extra) {
        this.extras.push(extra)
    }
}

export default Ticket;
