# Cinema Tickets Exercise

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). \
IDE used for the development: [Jetbrains - WebStorm](https://www.jetbrains.com/webstorm/)
## Available Scripts

In the project directory, you can run:

### `npm start`

in order to run the local server.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
React generated files are included within the project.
---

## Development

The app uses for different classes in order to store the information and calculate final prices:

- `Booking` --  Tickets are saved within an array inside a Booking object, in order to process them easily. 
- `Ticket` --  Name, price and extras are contained in this object, though extras are a different object type.
- `Extra` -- Pretty simple object with just two fields: name and price. These objects are saved to an array into the tickets.
- `Offer` -- Conditions and discounts are contained in here. Two of it's values (`minimumTickets` and `weekday`) mark
the conditions a booking has to meet in order for that offer to be applied. On top of that, `chargedTickets` indicates
how many tickets would be affected by the offer and `discount` is a 0-100 value, indicating the discount that's applied.


## Key points


>1. Tickets can be added in any order

Tickets can bre created using it is constructor, passing the name of the ticket as the first parameter and the price for
that ticket. Optionally, a third parameter (`Extra` objects array) to add some extras to that particular ticket.

>2. Offers should always be applied if possible

Offers are applied invoking `Booking` method called `applyOffers(Array offers)`. Offers passed by parameter would be 
applied if they meet the requirements: a minimum amount of tickets and day of the week.

>3. The cinema owner would like to be able to show the customer how much they saved when offers have been applied

`Booking` class has a method called `getDiscount()` that returns the difference between total price with no discounts 
and final prices with offers applied.

>4. New types of pricing/offers may need to be added in the future\

`Offers` can be defined on `App.js` and added to the array. Offers would be applied in order of addition to thta array.
Only one `Offer` can be applied to a `Ticket`. 

## Notes about solution design

No UI was required on the test spec, although a basic `index.html` is provided. Data is defined
in `App.js`.


